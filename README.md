<!--
SPDX-FileCopyrightText: 2022 Florian Warzecha <florian_filip.warzecha@fh-bielefeld.de>

SPDX-License-Identifier: MIT
-->

# liketechnik's pandoc templates

pandoc templates for:

- presentations with [reveal.js](https://revealjs.com)
- thesis and similar documents (based on [cagix/pandoc-thesis](https://github.com/cagix/pandoc-thesis) and [derric/cleanthesis](https://github.com/derric/cleanthesis))
- simple pdf documents with sane defaults (especially usefull in combination with presentations to generate both from the same markdown file)
- beautiful font(s) with [Recursive](https://www.recursive.design/)

features:

- sane defaults but (hopefully) still customizable
- combination of presentations + pdf from the same sourcefile possible
- Makefile to run the required commands

## Usage

- don't forget to specify `lang: de-DE` or `lang: en-US` (or whatever fits your the language of your document) in your metadata file

## Bugs

- Smallcaps fonts in latex documents: Unfortunately Recursive does (currently) not have a Smallcaps variant. For more details see [#1](https://gitlab.com/liketechnik/liketechniks-pandoc-templates/-/issues/1).
