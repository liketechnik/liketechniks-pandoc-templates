-- SPDX-FileCopyrightText: 2022 Florian Warzecha <florian_filip.warzecha@fh-bielfeld.de>
--
-- SPDX-License-Identifier: MIT

if FORMAT:match 'latex' then
  function Header (elem)
      if elem.attributes["presentation-only"] then
          return {}
      else
          return elem
      end
  end
end

if FORMAT:match 'revealjs' then
  function Header (elem)
    return nil -- unchanged 
  end
end
